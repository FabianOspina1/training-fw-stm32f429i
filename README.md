﻿# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
    contains training on the following topics
    STM32 MCUs
        FW - Training - 02 - ARM in STM32: Processor and peripherals mapping
        FW - Training - 03 - STM32CUBEMX
        FW - Training - 04 - STM32 HAL (High Abstraction Layer)
        FW - Training - 05 - System init (HAL, Clock)
        FW - Training - 06 - Peripherals init (GPIO, ADC, UART)
        FW - Training - 07 - Debug (UART, USB)
        FW - Training - 08 - Polling.
        FW - Training - 09 - Interruption and callbacks. ->  STM32F4 1.3  
        FW - Training - 10 - Startup.
        FW - Training - 11 - Communications interfaces (UART, SPI, I2C, ...) -> STM32F4 2.0     
        FW - Training - 12 - DMA. -> STM32F4 3.0 and STM32F4 3.1
        FW - Training - 13 - Debugging techniques. -> STM32F4 1.4     
        FW - Training - 14 - Assert (usage)


* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Fabian Andres Ospina Rodriguez
* Titoma